<?php

return [
	/* Sub Category */

	'SUB_CATEGORY_ADDED' => 'SubCategory added successfully.',
	'SUB_CATEGORY_EDITED' => 'SubCategory edited successfully.',
	'SUB_CATEGORY_DELETE' => 'SubCategory deleted successfully.',
	'BARBIELEGS_ADDED' => 'Barbielegs added successfully.',
	'BARBIELEGS_EDITED' => 'Barbielegs edited successfully.',
	'BARBIELEGS_DELETE' => 'Barbielegs deleted successfully.',
	'STAYINGFIT_ADDED' => 'Stayingfit added successfully.',
	'STAYINGFIT_EDITED' => 'Stayingfit edited successfully.',
	'STAYINGFIT_DELETE' => 'Stayingfit deleted successfully.',
	'NUTRITION_ADDED' => 'Nutrition added successfully.',
	'NUTRITION_EDITED' => 'Nutrition edited successfully.',
	'NUTRITION_DELETE' => 'Nutrition deleted successfully.',
	'SOMETHING_WENT_WRONG' => 'Something went wrong!',
	'EMAIL_NOT_REGISTERED' => "We can't find a user with that e-mail address.",
	'ABOUT_US_ADDED' => "About us added successfully.",
	'FITMOMTOBE_ADDED' => "Fit Mom-2-B added successfully.",
	'FITMOMTOBE_EDITED' => 'Fit Mom-2-B edited successfully.',
	'FITMOMTOBE_DELETE' => 'Fit Mom-2-B deleted successfully.',
];