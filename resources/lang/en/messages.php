<?php

return [
	'SIGN_UP_SUCCESS' => 'Thanks for signing up! Please check your email to complete your registration.',
	//'VERYFY_EMAIL_ERROR' => 'We cant find an account with this credentials. Please make sure you entered the right information and you have verified your email address.',
	'VERYFY_EMAIL_ERROR' => 'Invalid credentials.',
	'LOGIN_FAILED' => 'Failed to login, please try again.',
	'LOGIN_SUCCESS' => 'You are successfully login.',
	'LOGOUT_SUCCESS' => 'You have successfully logged out.',
	'LOGOUT_FAIL' => 'Failed to logout, please try again..',
	'INVALID_TOKEN' => 'Invalid token.',
	'EXPIRED_TOKEN' => 'Token is Expired.',
	'TOKEN_NOT_FOUND' => 'Authorization Token not found.',
	'EMAIL_NOT_FOUND' => 'Your email address was not found.',
	'RESET_PASSWORD_LINK' => 'A reset email has been sent! Please check your email.',
	'VERIFIED_CODE_INVALID' => 'Invalid Verification code.',
	'ACCOUNT_ALREADY_VERIFIED' => 'Account already verified..',
	'SUCCESSFULLY_VERYFIED' => 'You have successfully verified your email address.',
	'OLD_PASSWORD_NOT_MATCH' => 'Old password doesnt match!',
	'SOMETHING_WENT_WRONG' => 'Something went wrong!',
	'PASSWORD_CHANGE_SUCCESSFULLY' => 'Password change successfully.',
	'USER_DETAIL' => 'User Detail.',
	'PROFILE_UPDATE' => 'Profile update successfully.',
	'PAGE_DETAIL' => 'Page detail.',
	'PAGE_NOT_FOUND' => 'Page not found.',


	/* Category */
	'CATEGORY_LIST' => 'Category List.',
	'CATEGORY_LIST_NOT_AVAILABLE' => 'Category list not available.',
	'NUTRITION_CATEGORY_LIST' => 'Nutrition category list.',
	'NUTRITION_CATEGORY_LIST_NOT_AVAILABLE' => 'Nutrition category list not available.',
	'NUTRITION_LIST' => 'Nutrition list.',
	'NUTRITION_LIST_NOT_AVAILABLE' => 'Nutrition list not available.',
	'NUTRITION_DETAIL' => 'Nutrition detail.',
	'NUTRITION_DETAIL_NOT_AVAILABLE' => 'Nutrition detail not available.',
	'STAYING_FIT_LIST' => 'Staying fit list.',
	'STAYING_FIT_LIST_NOT_AVAILABLE' => 'Staying fit list not available.',
	'BARBIELEGS_LIST' => 'Barbielegs list.',
	'BARBIELEGS_LIST_NOT_AVAILABLE' => 'Barbielegs list not available.',
	'FIT_MOM_2_B_LIST' => 'Fit Mom-2-B list.',
	'FIT_MOM_2_B_LIST_NOT_AVAILABLE' => 'Fit Mom-2-B list not available.',

];