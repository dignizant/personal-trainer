@if(Session::has('Success'))
    <div class="alert alert-success" id="success-alert">
        <span class="glyphicon glyphicon-ok"></span>
        <em> {!! session('Success') !!}</em>
    </div>
@endif
@if(Session::has('Error'))
    <div class="alert alert-danger" id="danger-alert">
        <span class="glyphicon glyphicon-ok"></span>
        <em> {!! session('Error') !!}</em>
    </div>
@endif
@if(Session::has('Warning'))
    <div class="alert alert-warning" id="warning-alert">
        <span class="glyphicon glyphicon-ok"></span>
        <em> {!! session('Warning') !!}</em>
    </div>
@endif