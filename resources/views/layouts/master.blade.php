<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<?php 
    $current_route = explode('.',Route::currentRouteName())[0];

    $category_active = $nutrition_active = $staying_fit_active = $barbielege_active = $users_active = $pages_active = $fit_mom_to_be_active = '';
    if ($current_route == 'sub-category') {
        $category_active = 'active';
    } else if ($current_route == 'nutrition') {
        $nutrition_active = 'active';
    } else if ($current_route == 'staying_fits') {
        $staying_fit_active = 'active';
    } else if ($current_route == 'barbielegs') {
        $barbielege_active = 'active';
    } else if ($current_route == 'users') {
        $users_active = 'active';
    } else if ($current_route == 'pages') {
        $pages_active = 'active';
    } else if ($current_route == 'fit_mom_2_b') {
        $fit_mom_to_be_active = 'active';
    }
?>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>{{ config('app.name', 'Laravel') }}</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{url('admin/bower_components/bootstrap/dist/css/bootstrap.min.css')}}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{url('admin/bower_components/font-awesome/css/font-awesome.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="{{url('admin/bower_components/Ionicons/css/ionicons.min.css')}}">
        <!-- DataTables -->
        <link rel="stylesheet" href="{{url('admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('admin/dist/css/AdminLTE.min.css')}}">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
            folder instead of downloading all of them to reduce the load. -->
        <link rel="stylesheet" href="{{url('admin/dist/css/skins/_all-skins.min.css')}}">
        <!-- Morris chart -->
        <link rel="stylesheet" href="{{url('admin/bower_components/morris.js/morris.css')}}">
        <!-- jvectormap -->
        <link rel="stylesheet" href="{{url('admin/bower_components/jvectormap/jquery-jvectormap.css')}}">
        <!-- Date Picker -->
        <link rel="stylesheet" href="{{url('admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css')}}">
        <!-- sweet alert -->
        <link href="{{url('admin/bower_components/bootstrap-sweetalert/sweet-alert.css')}}" rel="stylesheet" type="text/css" />
        <!-- Daterange picker -->
        <link rel="stylesheet" href="{{url('admin/bower_components/bootstrap-daterangepicker/daterangepicker.css')}}">
        <!-- bootstrap wysihtml5 - text editor -->
        <link rel="stylesheet" href="{{url('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css')}}">
        <!-- custome css -->
        <link rel="stylesheet" href="{{url('admin/css/style.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{url('admin/bower_components/select2/dist/css/select2.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{url('admin/dist/css/AdminLTE.min.css')}}">
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <!-- jQuery 3 -->
        <script src="{{url('admin/bower_components/jquery/dist/jquery.min.js')}}"></script>
        <script src="{{url('admin/bower_components/jquery-ui/jquery-ui.min.js')}}"></script>
         <!-- Bootstrap 3.3.7 -->
         <script src="{{url('admin/bower_components/bootstrap/dist/js/bootstrap.min.js')}}"></script>
        <script>
            var site_url = "{{ url('/')}}";
        </script>
    </head>
    <body class="hold-transition skin-blue sidebar-mini">
        <div class="wrapper">
            <header class="main-header">
                <!-- Logo -->
            <a href="{{ url('home')}}" class="logo">
                <!-- mini logo for sidebar mini 50x50 pixels -->
                <span class="logo-mini">Personal Trainer</span>
                <!-- logo for regular state and mobile devices -->
                <span class="logo-lg">Personal Trainer</span>
                </a>
                <!-- Header Navbar: style can be found in header.less -->
                <nav class="navbar navbar-static-top">
                    <!-- Sidebar toggle button-->
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <span class="sr-only">Toggle navigation</span>
                    </a>

                    <div class="navbar-custom-menu">
                        <ul class="nav navbar-nav">
                            <!-- User Account: style can be found in dropdown.less -->
                            <li class="dropdown user user-menu">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                    <!-- <img src="{{url('admin/dist/img/user2-160x160.jpg')}}" class="user-image" alt="User Image"> -->
                                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                                </a>
                                <ul class="dropdown-menu dropdown-custome-width">
                                    <!-- Menu Footer-->
                                    <li class="user-footer">
                                        {{-- <a href="#" class="btn btn-flat">Profile</a> --}}
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();" class="btn btn-flat">Sign out</a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </nav>
            </header>
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="main-sidebar">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu" data-widget="tree">
                        {{-- <li class="header">MAIN NAVIGATION</li> --}}
                        <li class ="{{$category_active}}">
                            <a href="{{url('sub-category')}}">
                                <i class="fa fa-th"></i> <span>Sub Category</span>
                            </a>
                        </li>
                        <li class ="{{$users_active}}">
                            <a href="{{url('users')}}">
                                <i class="fa fa-users"></i> <span>	Users</span>
                            </a>
                        </li>
                        <li class ="{{$nutrition_active}}">
                            <a href="{{url('nutrition')}}">
                                <i class="fa fa-th"></i> <span>	Nutrition</span>
                            </a>
                        </li>
                        <li class ="{{$staying_fit_active}}">
                            <a href="{{url('staying_fits')}}">
                                <i class="fa fa-th"></i> <span>	Staying Fit</span>
                            </a>
                        </li>
                        <li class ="{{$barbielege_active}}">
                            <a href="{{url('barbielegs')}}">
                                <i class="fa fa-th"></i> <span>	Barbielegs</span>
                            </a>
                        </li>
                        <li class ="{{$fit_mom_to_be_active}}">
                            <a href="{{url('fit_mom_2_b')}}">
                                <i class="fa fa-th"></i> <span>	Fit Mom-2-B</span>
                            </a>
                        </li>
                        <li class ="{{$pages_active}}">
                            <a href="{{url('pages')}}">
                                <i class="fa fa-th"></i> <span>	About us</span>
                            </a>
                        </li>
                        {{-- <li>
                            <a href="https://adminlte.io/docs">
                                <i class="fa fa-book"></i> 
                                <span>Documentation</span>
                            </a>
                        </li> --}}
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- main contant -->
                @yield('content')
            </div>
            <!-- /.content-wrapper -->
            <footer class="main-footer">
                <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
                reserved.
            </footer>
        </div>
        <!-- ./wrapper -->

        
        <!-- jQuery UI 1.11.4 -->
        
        <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
        <script>
        $.widget.bridge('uibutton', $.ui.button);
        </script>
        <!-- Jquery validation -->
       <script src="{{ asset('js/jquery/jquery.validate.min.js') }}" ></script>
       <script src="{{ asset('js/jquery/additional-methods.min.js') }}" ></script>
        <!-- DataTables -->
        <script src="{{url('admin/bower_components/datatables.net/js/jquery.dataTables.min.js')}}"></script>
        <script src="{{url('admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js')}}"></script>
        <!-- Select2 -->
        <script src="{{url('admin/bower_components/select2/dist/js/select2.full.min.js')}}"></script>
        <!-- Morris.js charts -->
        <script src="{{url('admin/bower_components/raphael/raphael.min.js')}}"></script>
        <script src="{{url('admin/bower_components/morris.js/morris.min.js')}}"></script>
        <!-- Sparkline -->
        <script src="{{url('admin/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js')}}"></script>
        <!-- jvectormap -->
        <script src="{{url('admin/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js')}}"></script>
        <script src="{{url('admin/plugins/jvectormap/jquery-jvectormap-world-mill-en.js')}}"></script>
        <!-- jQuery Knob Chart -->
        <script src="{{url('admin/bower_components/jquery-knob/dist/jquery.knob.min.js')}}"></script>
        <!-- daterangepicker -->
        <script src="{{url('admin/bower_components/moment/min/moment.min.js')}}"></script>
        <script src="{{url('admin/bower_components/bootstrap-daterangepicker/daterangepicker.js')}}"></script>
        <!-- datepicker -->
        <script src="{{url('admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js')}}"></script>
        <!-- Bootstrap WYSIHTML5 -->
        <script src="{{url('admin/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js')}}"></script>
        <!-- Slimscroll -->
        <script src="{{url('admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js')}}"></script>
        <!-- FastClick -->
        <script src="{{url('admin/bower_components/fastclick/lib/fastclick.js')}}"></script>

        <!-- sweet alert -->
        <script src="{{url('admin/bower_components/bootstrap-sweetalert/sweet-alert.min.js')}}"></script>
        <!-- AdminLTE App -->
        <script src="{{url('admin/dist/js/adminlte.min.js')}}"></script>
        <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
        <!-- <script src="{{url('admin/dist/js/pages/dashboard.js')}}"></script> -->
        <!-- AdminLTE for demo purposes -->
        <script src="{{url('admin/dist/js/demo.js')}}"></script>
        <!-- custome js -->
        <script src="{{url('admin/js/script.js')}}"></script>
        <script>
            $(function () {
                $('#example2').DataTable({
                'paging'      : true,
                'lengthChange': false,
                'searching'   : false,
                'ordering'    : true,
                'info'        : true,
                'autoWidth'   : false
                })
            })
            </script>
    </body>
</html>
