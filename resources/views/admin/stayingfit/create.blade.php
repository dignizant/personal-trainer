@extends('layouts.master')
@section('content')
@include('layouts.error')
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="row">
      <div class="col-md-2"></div>
      <div class="col-md-8">
        <div class="box box-default">
            <div class="box-header with-border m-b-20">
                <h3 class="box-title">
                <span class="box-tools">
                    <a href="{{url('staying_fits')}}"><button type="button" class="btn btn-primary">< Back</button></a>
                </span> &nbsp;
                Create StayingFits</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{url('staying_fits')}}" enctype="multipart/form-data" class="form_submit" id="staying_fits_form" name="staying_fits_form" method="POST">
            {{ csrf_field() }}
                <div class="box-body">
                   <div class="col-md-12">
                        <div class="form-group">
                            <label>Sub Category</label>
                            <div class="">
                                <select class="form-control select2" name="sub_category_id" id="sub_category_id">
                                    <option value="">Select one</option>
                                    @foreach($subCategories as $category)
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('sub_category_id'))
                                <div class="error">{{ $errors->first('sub_category_id') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Title</label>
                            <input type = "text" name="title" id="title" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                            <div class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>StayingFits Video</label>
                            <input type='file' id="video" name="video" accept=".mp4" />
                            <input type='hidden' id="video_path" name="video_path" value="" />
                            <span class="error" id="video-error"></span>
                        </div>
                        @if ($errors->has('video'))
                            <div class="error">{{ $errors->first('video') }}</div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Status</label>
                            <div>
                                <select class="form-control select2" name="status" id="status">
                                    <option value="active">Active</option>
                                    <option value = "inactive">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
<section>
<script src="{{ asset('js/ajax/staying_fits/create.js') }}"></script>
@endsection



