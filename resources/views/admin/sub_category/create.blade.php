@extends('layouts.master')
@section('content')
@include('layouts.error')
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="row">
        <div class="col-md-2"></div>
        <div class="col-md-8">
            <div class="box box-default">
                <div class="box-header with-border">
                    <h3 class="box-title">
                    <span class="box-tools">
                        <a href="{{url('sub-category')}}"><button type="button" class="btn btn-primary">< Back</button></a>
                    </span> &nbsp;
                    Create Sub Category</h3>
                </div>
                <!-- /.box-header -->
                <form action="{{url('sub-category')}}" class="form_submit" id="sub-category-form" method="POST">
                {{ csrf_field() }}
                    <div class="box-body">
                        <div class="col-md--12">
                            <div class="form-group">
                                <label>Category</label>
                                <div class="">
                                    <select class="form-control select2" name="category_id" id="category_id">
                                        <option value="">Select one</option>
                                        @foreach($categories as $category)
                                        <option value="{{$category->id}}">{{$category->category_name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                @if ($errors->has('category_id'))
                                    <div class="error">{{ $errors->first('category_id') }}</div>
                                @endif
                            </div>
                        </div>
                        <div class="col-md--12">
                            <div class="form-group">
                                <label>Sub Category</label>
                                <input type = "text" name="category_name" id="category_name" class="form-control">
                            </div>
                            @if ($errors->has('category_name'))
                                <div class="error">{{ $errors->first('category_name') }}</div>
                            @endif
                        </div>
                        <div class="col-md--12">
                            <div class="form-group">
                                <label>Status</label>
                                <div>
                                    <select class="form-control select2" name="status" id="status">
                                        <option value="active">Active</option>
                                        <option value = "inactive">Inactive</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md--12">
                            <hr class="hr-line">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
<section>
<script src="{{ asset('js/ajax/subcategory/create.js') }}"></script>
@endsection



