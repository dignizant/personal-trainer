@extends('layouts.master')

@section('content')

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        @include('layouts.error')
      </div>
        <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Sub Categories</h3>
              <a href="{{url('sub-category/create')}}"><button class="btn btn-primary pull-right">Add New</button></a>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="data-table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Category Name</th>
                    <th>Main Category Name</th>
                    <th>Status</th>
                    <th width="100px">Action</th>
                  </tr>
                </thead>
                <tbody>
              
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
    </div>
</section>
<script type="text/javascript">
  $(function () {
    var table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "<?php echo url('sub-category'); ?>",
        columns: [
            {data: 'category_name', name: 'category_name'},
            {data: 'main_category_name', name: 'main_category_name'},
            {data: 'status', name: 'status'},
            {data: 'action', name: 'action', orderable: false, searchable: false},
        ]
    });
  });
</script>
@endsection
