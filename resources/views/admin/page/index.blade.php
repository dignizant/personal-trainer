@extends('layouts.master')
@section('content')
@include('layouts.error')
<div id="about_success"></div>
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-default">
            <div class="box-header with-border m-b-20">
                <h3 class="box-title">
                <!-- <span class="box-tools">
                    <a href="{{url('pages')}}"><button type="button" class="btn btn-primary">< Back</button></a>
                </span> &nbsp;-->
                About us</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{url('pages')}}" enctype="multipart/form-data" class="form_submit" id="pages_form" method="POST">
            {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Title</label>
                            <input type = "text" name="title" id="title" value="{{$pages['title']}}" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                            <div class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div>                   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <!-- <textarea name="description" id="description" class="form-control" col="5" rows="5"></textarea> -->
                            <div id="editor">
                            <textarea class="form-control ckeditor" name="description" id="description">
                                {{$pages['description']}}
                            </textarea>
                            </div>
                        </div>
                        @if ($errors->has('description'))
                            <div class="error">{{ $errors->first('description') }}</div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="btnSave" name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
<section>
<script>
    default_url = "{{url('/pages')}}";
    /*img_url = "{{url('/images/add_image.png')}}";
    loader_image = "{{url('/images/image_loader.gif')}}";
    crop_modal_url = "{{url('/ajax_get_crop_modal')}}";
    remove_image_url = "{{url('/pages/remove_image')}}";*/
    csrf_token = "{{csrf_token() }}";
    post_method = "POST";
</script>
<script src="{{ asset('admin/plugins/ckeditor/ckeditor.js') }}"></script>
<script src="{{ asset('js/ajax/pages/create.js') }}"></script>
@endsection



