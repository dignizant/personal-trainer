@extends('layouts.master')

@section('content')

<section class="content">
    <div class="row">
      <div class="col-xs-12">
        @include('layouts.error')
      </div>
        <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
              <h3 class="box-title">Users</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="data-table" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Name</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
              
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
    </div>
</section>
<script type="text/javascript">
  $(function () {
    var table = $('#data-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: "<?php echo url('users'); ?>",
        columns: [
            {data: 'name', name: 'name'},
            {data: 'email', name: 'email'},
        ]
    });
  });
</script>
@endsection
