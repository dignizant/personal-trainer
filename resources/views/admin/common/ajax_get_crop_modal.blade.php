<link rel="stylesheet" href="{{url('admin/bower_components/cropper/cropper.css')}}">
<script src="{{url('admin/bower_components/cropper/cropper.js')}}">
</script>
<script>
	$(function ()
		{
			var $image = $('#image');
			var cropBoxData;
			var canvasData;

			$('#crop_image_modal').on('shown.bs.modal', function ()
			{
				$image.cropper(
				{
					viewMode: 1,
					aspectRatio: '<?php echo $data["ratioWidth"] ?>' / '<?php echo $data["rationHeight"] ?>' ,
					autoCropArea: 0.5,
					built: function ()
					{
						$image.cropper('setCanvasData', canvasData);
						$image.cropper('setCropBoxData', cropBoxData);
					}
				});
			});
				
			$(".close").click(function(){
				
				$("<?php echo $data['preview_image']; ?>").attr('src', '<?php echo $data["prev_image"]; ?>');
				$("<?php echo $data['input_image']; ?>").val('');
				$image.cropper('destroy');
				$('#crop_image_modal').modal('hide');
					
			});
				
			$(".btn_crop_done").click(function(){
				cropBoxData = $image.cropper('getCroppedCanvas');
				canvasData = $image.cropper('getCanvasData');
				//alert(cropBoxData.toDataURL());
				$("<?php echo $data['preview_image']; ?>").attr('src',cropBoxData.toDataURL("<?php echo $data['mimeType']; ?>"));
				$("<?php echo $data['hidden_input_image']; ?>").val(cropBoxData.toDataURL("<?php echo $data['mimeType']; ?>"));
				$image.cropper('destroy');
				$("#crop_image_modal").modal('hide');
			});
		});
</script>
<div class="modal fade" id="crop_image_modal" aria-labelledby="modalLabel" role="dialog" tabindex="-1">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-label="Close">
					<span aria-hidden="true">
						&times;
					</span>
				</button>
				<h4 class="modal-title" id="modalLabel">
					Crop the image
				</h4>
			</div>
			<div class="modal-body">
				<div>
					<img id="image" src="<?php echo $data['image_data']; ?>" width="100%" alt="Picture">
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn_crop_done">
					Done
				</button>
			</div>
		</div>
	</div>
</div>
