@extends('layouts.master')
@section('content')
@include('layouts.error')
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-default">
            <div class="box-header with-border m-b-20">
                <h3 class="box-title">
                <span class="box-tools">
                    <a href="{{url('nutrition')}}"><button type="button" class="btn btn-primary">< Back</button></a>
                </span> &nbsp;
                Create Recipe</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{url('nutrition')}}" enctype="multipart/form-data" class="form_submit" id="nutrition_form" method="POST">
            {{ csrf_field() }}
                <div class="box-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Title</label>
                            <input type = "text" name="title" id="title" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                            <div class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Duration (Minutes)</label>
                            <input type = "number" name="duration" id="duration" class="form-control">
                        </div>
                        @if ($errors->has('duration'))
                            <div class="error">{{ $errors->first('duration') }}</div>
                        @endif
                    </div>                   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="description" class="form-control" col="5" rows="5"></textarea>
                        </div>
                        @if ($errors->has('description'))
                            <div class="error">{{ $errors->first('description') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sub Category</label>
                            <div class="">
                                <select class="form-control select2" name="sub_category_id" id="sub_category_id">
                                    <option value="">Select one</option>
                                    @foreach($subCategories as $category)
                                    <option value="{{$category->id}}">{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('sub_category_id'))
                                <div class="error">{{ $errors->first('sub_category_id') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <div>
                                <select class="form-control select2" name="status" id="status">
                                    <option value="active">Active</option>
                                    <option value = "inactive">Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-12">
                            <?php $img_count = 0;?>
                            <h3 class="box-title">Recipe Images
                            <a href="javascript:void(0);"><button type="button" disabled="disabled" id="btnAddMore" name="btnAddMore" class="btn btn-primary pull-right">Add New</button></a></h3>
                        </div>
                        <br>
                        <div class="form-group">
				            <div class="all_images">
				              <div class="col-md-2" id="newDivImg0">
				                <div class="img_hover_edit image-item">
				                  <input type="file" class="picture_input0" onchange="display_image(this,'0')" name="picture_input0" value="" />
				                  <input  type="hidden" id="hdn_picture0" name="picture[]">
				                  <input  type="hidden" name="images_id[]" id="hdn_images_id0" value="0" />
				                  <input type="hidden" id="hidPreImg0" name="hidPreImg0" value="{{url('/images/add_image.png')}}"/>
				                  <a class="image-item-hover edit" id="editImg0" style="display:none"><span class="fa fa-edit fa-5x"></span></a>
				                  <img src="{{url('/images/add_image.png')}}" id="preview_picture0" alt="Alternate Text" />
				                </div>
				              </div>
				            </div>
				            <div class="col-md-12">
				            	<label id="hdn_picture0-error" class="error" for="hdn_picture0" style="display: none;"></label>
			              	</div>
				        </div>
				          <input type="hidden" id="hidforBuffered" name="hidforBuffered" value="<?php echo $img_count;?>">
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="btnSave" name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
    <div id="crop_modal_container"></div>
<section>
<script>
    default_url = "{{url('/nutrition')}}";
    img_url = "{{url('/images/add_image.png')}}";
    loader_image = "{{url('/images/image_loader.gif')}}";
    crop_modal_url = "{{url('/ajax_get_crop_modal')}}";
    remove_image_url = "{{url('/nutrition/remove_image')}}";
    csrf_token = "{{csrf_token() }}";
    post_method = "POST";
</script>
<script src="{{ asset('js/ajax/nutrition/create.js') }}"></script>
@endsection



