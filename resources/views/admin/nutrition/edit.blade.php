@extends('layouts.master')
@section('content')
@include('layouts.error')
<?php 
    $image_array = array_column($nutrition['recipie_images'], 'image');
    $images_id = array_column($nutrition['recipie_images'], 'id');
?>
<section class="content">
    <!-- SELECT2 EXAMPLE -->
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-10">
        <div class="box box-default">
            <div class="box-header with-border m-b-20">
                <h3 class="box-title">
                <span class="box-tools">
                    <a href="{{url('nutrition')}}"><button type="button" class="btn btn-primary">< Back</button></a>
                </span> &nbsp;
                Edit Recipe</h3>
            </div>
            <!-- /.box-header -->
            <form action="{{url('nutrition/'.$nutrition['id'])}}" enctype="multipart/form-data" class="form_submit" id="nutrition_form" method="POST">
                <div class="box-body">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Title</label>
                        <input type = "text" name="title" id="title" value="{{$nutrition['title']}}" class="form-control">
                        </div>
                        @if ($errors->has('title'))
                            <div class="error">{{ $errors->first('title') }}</div>
                        @endif
                    </div> 
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Duration (Minutes)</label>
                            <input type = "number" name="duration" value="{{$nutrition['duration']}}"  id="duration" class="form-control">
                        </div>
                        @if ($errors->has('duration'))
                            <div class="error">{{ $errors->first('duration') }}</div>
                        @endif
                    </div>                   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <textarea name="description" id="description" class="form-control" col="5" rows="5">{{$nutrition['description']}}</textarea>
                        </div>
                        @if ($errors->has('description'))
                            <div class="error">{{ $errors->first('description') }}</div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Sub Category</label>
                            <div class="">
                                <select class="form-control select2" name="sub_category_id" id="sub_category_id">
                                    <option value="">Select one</option>
                                    @foreach($subCategories as $category)
                                    <option value="{{$category->id}}" {{($nutrition['sub_category_id'] == $category->id) ? 'selected' : ''}}>{{$category->category_name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @if ($errors->has('sub_category_id'))
                                <div class="error">{{ $errors->first('sub_category_id') }}</div>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Status</label>
                            <div>
                                <select class="form-control select2" name="status" id="status">
                                    <option value="active" {{($nutrition['sub_category_id'] == 'active') ? 'selected' : ''}}>Active</option>
                                    <option value = "inactive" {{($nutrition['sub_category_id'] == 'inactive') ? 'selected' : ''}}>Inactive</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <div class="col-md-12">
                            <?php $img_count = 0;?>
                            <h3 class="box-title">Recipe Images
                            <a href="javascript:void(0);"><button type="button" id="btnAddMore" {{(count($image_array) <= 0) ? "disabled='disabled'" : "" }} name="btnAddMore" class="btn btn-primary pull-right">Add New</button></a></h3>
                        </div>
                        <br>
                        <div class="form-group">
				            <div class="all_images">
                                <?php $img_len = count($image_array);
                                $image_url   = url('/images/add_image.png');
                                if ($img_len <= 0) { ?>
                                    <div class="col-md-2" id="newDivImg0">
                                        <div class="img_hover_edit image-item">
                                            <input type="file" class="picture_input0" onchange="display_image(this,'0')" name="picture_input0" value="" />
                                            <input  type="hidden" id="hdn_picture0" name="picture[]">
                                            <input  type="hidden" name="images_id[]" id="hdn_images_id0" value="0" />
                                            <input type="hidden" id="hidPreImg0" name="hidPreImg0" value="{{url('/images/add_image.png')}}"/>
                                            <a class="image-item-hover edit" id="editImg0" style="display:none"><span class="fa fa-edit fa-5x"></span></a>
                                            <img src="{{url('/images/add_image.png')}}" id="preview_picture0" alt="Alternate Text" />
                                        </div>
                                    </div>
                                <?php } else {
                                    for($i=0; $i<$img_len; $i++) 
                                { 
                                    $image = $image_array[$i];
                                    $type = pathinfo($image, PATHINFO_EXTENSION);
                                    if(file_exists(public_path().'/uploaded_images/Nutrition/'.$image)) {
                                        $data = file_get_contents(public_path().'/uploaded_images/Nutrition/'.$image);
                                        if($image != '')  {
                                            $img = 'data:image/' . $type . ';base64,' . base64_encode($data);
                                            $image_url = ($image_array[$i] != '') ? $img : $image_url;
                                        }
                                    }
                                    else {
                                        $image_url   = url('/images/no_image.png');
                                    }
                                    ?>
                                    <div class="col-md-2" id="newDivImg<?php echo $i;?>">
                                        <div class="img_hover_edit image-item">
                                          <input type="file" class="picture_input<?php echo $i;?>" onchange="display_image(this,'<?php echo $i;?>')" name="picture_input<?php echo $i;?>" value="" />
                                          <input type="hidden" id="hdn_picture<?php echo $i;?>" name="picture[]" value="<?php echo (@$image_array[$i] != '') ? @$image_url : '';?>">
                                          <input type="hidden" name="images_id[]" id="hdn_images_id<?php echo $i;?>" value="<?php echo @$images_id[$i]; ?>" />
                                          <input type="hidden" id="hidPreImg<?php echo $i;?>" name="hidPreImg<?php echo $i;?>" value="<?php echo @$image_url?>"/>
                                          <a class="image-item-hover edit" id="editImg<?php echo $i;?>" style="display:<?php echo (@$image_array[$i] != '') ? 'block' : 'none';?>"><span class="fa fa-edit fa-5x"></span></a>
                                          <img src="<?php echo @$image_url; ?>" id="preview_picture<?php echo $i;?>" alt="Alternate Text" />
                                          <?php if ($i != '0') {?>
                                            <a id="del_btn<?php echo $i?>" data-key="<?php echo $i?>" data-id="<?php echo @$images_id[$i]; ?>" class="del_icon close" onclick="remove_image('<?php echo @$images_id[$i]; ?>', '<?php echo $i?>')">
                                              <i class='fa fa-times'></i>
                                            </a>
                                          <?php } ?>
                                        </div>
                                    </div>
                                <?php $img_count++; } 
                                } ?>
				            </div>
				            <div class="col-md-12">
				            	<label id="hdn_picture0-error" class="error" for="hdn_picture0" style="display: none;"></label>
			              	</div>
				        </div>
				          <input type="hidden" id="hidforBuffered" name="hidforBuffered" value="<?php echo $img_count;?>">
                    </div>
                    <div class="col-md-12">
                        <hr>
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary" id="btnSave" name="submit">Submit</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
      </div>
    </div>
    <div id="crop_modal_container"></div>
<section>
<script>
    default_url = "{{url('/nutrition')}}";
    img_url = "{{url('/images/add_image.png')}}";
    loader_image = "{{url('/images/image_loader.gif')}}";
    crop_modal_url = "{{url('/ajax_get_crop_modal')}}";
    remove_image_url = "{{url('/nutrition/remove_image')}}";
    csrf_token = "{{csrf_token() }}";
    post_method = "PATCH";
</script>
<script src="{{ asset('js/ajax/nutrition/create.js') }}"></script>
@endsection



