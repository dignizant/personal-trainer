<?php 
namespace App\Models\Traits;


trait CommonModel{


    public function getDurations($duration)
    {
        
        $hours = floor($duration / 3600);
        $minutes = floor(($duration / 60) % 60);
        $seconds = $duration % 60;
        $duration_string = ' ';
        if ($hours > 0) {
            $duration_string = $hours." hour";
        } if ($minutes > 0) {
            $duration_string = " ".$minutes." min";
        } if ($seconds > 0) {
            $duration_string = " ".$seconds." sec";
        }
        return $duration_string;
    }

}
