<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\CommonModel;
class StayingFit extends Model
{
    //
    use CommonModel;
    protected $fillable = [
        'sub_category_id', 'title','video','video_thumb', 'status'
    ];
    protected $table = 'staying_fits';

    public function category_details()
    {
        return $this->belongsTo('App\SubCategory','sub_category_id','id');
    }

    public function getDurationAttribute($duration) {
        return $this->getDurations($duration);
    }
}
