<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\Nutrition;
use App\http\Requests;
use Illuminate\Http\Request;
use App\SubCategory;
use App\RecipeImage;
use Illuminate\Support\Facades\DB;
use Session;

class NutritionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $nutrition = Nutrition::with(['category_details'])->get();
            return Datatables::of($nutrition)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="nutrition/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a>
                                <form class="form-group" action="nutrition/'.$row->id.'" method="POST">'.method_field('DELETE').' '.csrf_field().'
                                <button type="submit" class="delete btn btn-danger btn-sm" style="border:none">Delete</button></form>';
                        return $btn;
                    })
                    ->addColumn('sub_category_name', function($row1){
                        return $row1->category_details->category_name;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.nutrition.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subCategories = SubCategory::where('category_id', 1)->where('status','active')->get();
        return view('admin.nutrition.create',compact('subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'sub_category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'duration' => 'required|numeric',
            'images' => 'required',
        ]);
        $all = $request->all();
        $files = $request->file('images');
    
        if($nutrition = Nutrition::create($all)->id) {
            if ($files = $request->file('images')) {
                $filePath = '/uploaded_images/Nutrition';
                if (!is_dir(public_path() . $filePath)) {
                    mkdir(public_path() . $filePath, 0777, true);
                }
                for ($i=0; $i < count($_FILES['images']['tmp_name']); $i++) { 
                    $image = $i.time() . ".png"; //. $_FILES['images']['tmp_name'][$i]->getClientOriginalExtension();
                    $status = move_uploaded_file($_FILES['images']['tmp_name'][$i], public_path() . $filePath.'/'.$image);
                    $recipe['recipe_id'] = $nutrition;
                    $recipe['image'] = $image;
                    $recipImages = RecipeImage::create($recipe);
                }
             }
            //return Redirect('nutrition')->with('Success', __('admin_message.NUTRITION_ADDED')); 
            Session::flash('Success',  __('admin_message.NUTRITION_ADDED'));
        } else {
            //return Redirect('nutrition')->with('Error', __('admin_message.SOMETHING_WENT_WRONG')); 
            Session::flash('Error',  __('admin_message.SOMETHING_WENT_WRONG'));
        }
        return '1';

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Nutrition  $nutrition
     * @return \Illuminate\Http\Response
     */
    public function show(Nutrition $nutrition)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Nutrition  $nutrition
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subCategories = SubCategory::where('category_id', 1)->where('status','active')->get();
        $nutrition = Nutrition::where('id',$id)->with(['recipie_images'])->first()->toArray();
        return view('admin.nutrition.edit',compact('subCategories','nutrition'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Nutrition  $nutrition
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Nutrition $nutrition)
    {
        //
        $this->validate($request, [
            'sub_category_id' => 'required',
            'title' => 'required',
            'description' => 'required',
            'duration' => 'required|numeric',
            'images' => 'required',
        ]);
        $all = $request->all();
        if($nutrition->update($all)) {
            if ($files = $request->file('images')) {
                $filePath = '/uploaded_images/Nutrition';
                if (!is_dir(public_path() . $filePath)) {
                    mkdir(public_path() . $filePath, 0777, true);
                }
                $image_id_array = explode(',',$all['image_id_array']);
                for ($i=0; $i < count($_FILES['images']['tmp_name']); $i++) { 
                    $image = $i.time() . ".png"; //. $_FILES['images']['tmp_name'][$i]->getClientOriginalExtension();
                    $status = move_uploaded_file($_FILES['images']['tmp_name'][$i], public_path() . $filePath.'/'.$image);
                    $recipe['recipe_id'] = $nutrition['id'];
                    $recipe['image'] = $image;
                    if ($image_id_array[$i] != 0) 
                    {
                        $recipeImages = RecipeImage::findOrFail($image_id_array[$i]);
                        $filePathDelete = '/uploaded_images/Nutrition/';
                        if (file_exists(public_path().$filePathDelete.$recipeImages['image'])) {
                            unlink(public_path().$filePathDelete.$recipeImages['image']);
                        }
                        $recipImages = $recipeImages->update($recipe);
                    } 
                    else 
                    {
                        $recipImages = RecipeImage::create($recipe);
                    }
                    
                }
             }
            //return Redirect('nutrition')->with('Success', __('admin_message.NUTRITION_EDITED')); 
            Session::flash('Success', __('admin_message.NUTRITION_EDITED'));
        } else {
            //return Redirect('nutrition')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
            Session::flash('Error',  __('admin_message.SOMETHING_WENT_WRONG'));
        }
        return '1';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Nutrition  $nutrition
     * @return \Illuminate\Http\Response
     */
    public function destroy(Nutrition $nutrition)
    {
        //
        $nutrition1 = Nutrition::where('id',$nutrition['id'])->with(['recipie_images'])->first()->toArray();
        $filePathDelete = '/uploaded_images/Nutrition/';
        foreach ($nutrition1['recipie_images'] as $val) {
            $recipeImages = RecipeImage::findOrFail($val['id']);
            //dd($recipeImages);
            if (file_exists(public_path().$filePathDelete.$recipeImages['image'])) {
                unlink(public_path().$filePathDelete.$recipeImages['image']);
            }
            $recipeImages->delete();
        }
        if($nutrition->delete()) {
            return Redirect('nutrition')->with('Success', __('admin_message.NUTRITION_DELETE'));
        } else {
            return Redirect('nutrition')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    public function removeImage($id) {
        $recipeImages = RecipeImage::findOrFail($id);
        $filePathDelete = '/uploaded_images/Nutrition/';
        if (file_exists(public_path().$filePathDelete.$recipeImages['image'])) {
            unlink(public_path().$filePathDelete.$recipeImages['image']);
        }
        $recipeImages->delete();
        echo '1';
    }
}
