<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\StayingFit;
use App\http\Requests;
use Illuminate\Http\Request;
use App\SubCategory;
use VideoThumbnail;
//use FFMpeg;

class StayingFitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $stayingfit = StayingFit::with(['category_details'])->get();
            return Datatables::of($stayingfit)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="staying_fits/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a>
                                <form class="form-group" action="staying_fits/'.$row->id.'" method="POST">'.method_field('DELETE').' '.csrf_field().'
                                <button type="submit" class="delete btn btn-danger btn-sm" style="border:none">Delete</button></form>';
                        return $btn;
                    })
                    ->addColumn('sub_category_name', function($row1){
                        return $row1->category_details->category_name;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.stayingfit.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subCategories = SubCategory::where('category_id', 2)->where('status','active')->get();
        return view('admin.stayingfit.create',compact('subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
    {
        //
        $this->validate($request, [
            'sub_category_id' => 'required',
            'title' => 'required',
            'video' => 'required|mimes:mp4'
        ]);
        $all = $request->all();
        
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Stayingfit';
            $thumbFilePath = '/uploaded_images/Stayingfit';
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $thumbnail = time() .'.png';
            $status = $files->move(public_path() . $filePath, $profilevideo);
            
            if ($status) {
                $img = VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath.'/', $thumbnail, 2, 640, 480);
               /* $ffprobe = FFMpeg\FFProbe::create([
                    'ffmpeg.binaries'  => env('FFMPEG','/usr/local/bin/ffmpeg/ffmpeg'),
                    'ffprobe.binaries' => env('FFMPEG','/usr/local/bin/ffmpeg/ffprobe')
                ]);
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";
         }
        if($stayingfit = StayingFit::create($all)) {
            return Redirect('staying_fits')->with('Success', __('admin_message.STAYINGFIT_ADDED'));
        } else {
            return Redirect('staying_fits')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\StayingFit  $stayingFit
     * @return \Illuminate\Http\Response
     */
    public function show(StayingFit  $stayingFit)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\StayingFit  $stayingFit
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $stayingfit = StayingFit::findOrFail($id);
        $subCategories = SubCategory::where('category_id', 2)->where('status','active')->get();
        return view('admin.stayingfit.edit',compact('subCategories','stayingfit'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\StayingFit  $stayingFit
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, StayingFit  $stayingfit)
    {
        //
        $validate_check_array = [
            'sub_category_id' => 'required',
            'title' => 'required',
        ];
        $all = $request->all();
        if (!$request->file('video') && $all['video_path'] == '') {
            $validate_check_array['video'] = 'required|mimes:mp4';
        } 
        $this->validate($request, $validate_check_array);
        $stayingfitOld = $stayingfit = StayingFit::findOrFail($id);
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Stayingfit';
            $thumbFilePath = '/uploaded_images/Stayingfit';
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }
            $thumbnail = time() .'.png';
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $status = $files->move(public_path() . $filePath, $profilevideo);

            if ($status) {
                $img = VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath.'/', $thumbnail, 2, 640, 480);
                /*$ffprobe = FFMpeg\FFProbe::create([
                    'ffmpeg.binaries'  => env('FFMPEG','/usr/local/bin/ffmpeg/ffmpeg'),
                    'ffprobe.binaries' => env('FFMPEG','/usr/local/bin/ffmpeg/ffprobe')
                ]);
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";

            if (file_exists(public_path().$filePath.'/'.$stayingfitOld['video'])) {
                unlink(public_path().$filePath.'/'.$stayingfitOld['video']);
            }
            if ($stayingfitOld['video_thumb'] && file_exists(public_path().$thumbFilePath.'/'.$stayingfitOld['video_thumb'])) {
                unlink(public_path().$thumbFilePath.'/'.$stayingfitOld['video_thumb']);
            }
        } else if ($all['video_path'] != '') {
            $all['video'] = $all['video_path'];
        }
       
        if($stayingfit->update($all)) {
            /*if($files = $request->file('video')) {
                if (file_exists(public_path().$filePath.'/'.$stayingfitOld['video'])) {
                    unlink(public_path().$filePath.'/'.$stayingfitOld['video']);
                }
                if ($stayingfitOld['video_thumb'] && file_exists(public_path().$thumbFilePath.'/'.$stayingfitOld['video_thumb'])) {
                    unlink(public_path().$thumbFilePath.'/'.$stayingfitOld['video_thumb']);
                }
            }*/
            return Redirect('staying_fits')->with('Success', __('admin_message.STAYINGFIT_EDITED'));
        } else {
            return Redirect('staying_fits')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\StayingFit  $stayingFit
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, StayingFit  $stayingfit)
    {
        //
        $stayingfit = StayingFit::find($id);
        $filePath = '/uploaded_videos/Stayingfit/';
        if (file_exists(public_path().$filePath.$stayingfit['video'])) {
            unlink(public_path().$filePath.$stayingfit['video']);
        }
        if($stayingfit->delete()) {
            return Redirect('staying_fits')->with('Success', __('admin_message.STAYINGFIT_DELETE'));
        } else {
            return Redirect('staying_fits')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }
}
