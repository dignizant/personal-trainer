<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\SubCategory;
use App\Category;
use App\http\Requests;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            //$subCategories = SubCategory::with(['category_details'])->get();
            $subCategories = SubCategory::all();
            return Datatables::of($subCategories)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="sub-category/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a>
                                <form class="form-group" action="sub-category/'.$row->id.'" method="POST">'.method_field('DELETE').' '.csrf_field().'
                                <button type="submit" class="delete btn btn-danger btn-sm" style="border:none">Delete</button></form>';
                        return $btn;
                    })
                    ->addColumn('main_category_name', function($row){
                        if ($row['category_id'] == '1') {
                            $category = 'Nutrition';
                        } else if ($row['category_id'] == '2') {
                            $category = 'Staying fit';
                        } else if ($row['category_id'] == '3') {
                            $category = 'Barbielegs';
                        } else {
                            $category = 'Fit Mom-2-B';
                        }
                        return $category;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
      
        //return view('users');
        return view('admin.sub_category.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.sub_category.create',compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'category_id' => 'required',
            'category_name' => 'required',
        ]);

        $all = $request->all();
       
        if($subCategory = SubCategory::create($all)) {
            return Redirect('sub-category')->with('Success', __('admin_message.SUB_CATEGORY_ADDED'));
        } else {
            return Redirect('sub-category')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function show(SubCategory $subCategory)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function edit(SubCategory $subCategory)
    {
        $categories = Category::all();

        return view('admin.sub_category.edit',compact('categories','subCategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SubCategory $subCategory)
    {
        //
        $this->validate($request, [
            'category_id' => 'required',
            'category_name' => 'required',
        ]);
        $category_update = SubCategory::findOrFail($subCategory['id']);
        if($category_update->update($request->all())) {
            return Redirect('sub-category')->with('Success', __('admin_message.SUB_CATEGORY_EDITED'));
        } else {
            return Redirect('sub-category')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SubCategory  $subCategory
     * @return \Illuminate\Http\Response
     */
    public function destroy(SubCategory $subCategory)
    {
        //
        $category = SubCategory::find($subCategory['id']);
        if($category->delete()) {
            return Redirect('sub-category')->with('Success', __('admin_message.SUB_CATEGORY_DELETE'));
        } else {
            return Redirect('sub-category')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }
}
