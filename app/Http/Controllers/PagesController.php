<?php

namespace App\Http\Controllers;

use Yajra\Datatables\Datatables;
use App\Pages;
use Illuminate\Http\Request;
use Session;

class PagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $pages = Pages::where('id', 1)->first()->toArray();
         //dd($pages);die;
        return view('admin.page.index',compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
        ]);

        $all = $request->all();
        $pages = Pages::find(1);
        if($pages) {
            $pages->title = $request->title;
            $pages->description = $request->description;
            if($pages->save()) {
                $result['success'] = __('admin_message.ABOUT_US_ADDED');
            }
            else {
                $result['error'] = __('admin_message.SOMETHING_WENT_WRONG');
            }
        }
        else {
            if($subCategory = Pages::create($all)) {
                $result['success'] = __('admin_message.ABOUT_US_ADDED');
            } else {
                $result['error'] = __('admin_message.SOMETHING_WENT_WRONG');
            }
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function show(Pages $pages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function edit(Pages $pages)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pages $pages)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pages  $pages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pages $pages)
    {
        //
    }
}
