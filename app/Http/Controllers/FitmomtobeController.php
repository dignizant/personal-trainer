<?php
namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\Fitmomtobe;
use App\http\Requests;
use Illuminate\Http\Request;
use App\SubCategory;
use VideoThumbnail;

class FitmomtobeController extends Controller
{
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $fitmomtobe = Fitmomtobe::with(['category_details'])->get();
            
            return Datatables::of($fitmomtobe)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="fit_mom_2_b/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a>
                                <form class="form-group" action="fit_mom_2_b/'.$row->id.'" method="POST">'.method_field('DELETE').' '.csrf_field().'
                                <button type="submit" class="delete btn btn-danger btn-sm" style="border:none">Delete</button></form>';
                        return $btn;
                    })
                    ->addColumn('sub_category_name', function($row1){
                        return $row1->category_details->category_name;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.fit_mom_to_be.index');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subCategories = SubCategory::where('category_id', 4)->where('status','active')->get();
        return view('admin.fit_mom_to_be.create',compact('subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sub_category_id' => 'required',
            'title' => 'required',
            'video' => 'required|mimes:mp4|max:61440'
        ]);
        $all = $request->all();
        // dd($all);
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Fitmomtobe';
            $thumbFilePath = '/uploaded_images/Fitmomtobe';
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $thumbnail = time() .'.png';
            $status = $files->move(public_path() . $filePath, $profilevideo);
            if ($status) {
                VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath.'/', $thumbnail, 2, 640, 480);
                /*$ffprobe = FFMpeg\FFProbe::create();
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";
        }
        if($fitmomtobe = Fitmomtobe::create($all)) {
            return Redirect('fit_mom_2_b')->with('Success', __('admin_message.FITMOMTOBE_ADDED'));
        } else {
            return Redirect('fit_mom_2_b')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Fitmomtobe  $fitmomtobe
     * @return \Illuminate\Http\Response
     */
    public function show(Fitmomtobe $fitmomtobe)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Fitmomtobe  $fitmomtobe
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Fitmomtobe $fitmomtobe)
    {
        $fitmomtobe = Fitmomtobe::findOrFail($id);
        $subCategories = SubCategory::where('category_id', 4)->where('status','active')->get();
        return view('admin.fit_mom_to_be.edit',compact('subCategories','fitmomtobe'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Fitmomtobe  $fitmomtobe
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Fitmomtobe $fitmomtobe)
    {
        $validate_check_array = [
            'sub_category_id' => 'required',
            'title' => 'required',
        ];
        $all = $request->all();
        if (!$request->file('video') && $all['video_path'] == '') {
            $validate_check_array['video'] = 'required|mimes:mp4';
        }
        $this->validate($request, $validate_check_array);
        $fitmomtobeOld = $fitmomtobe = Fitmomtobe::findOrFail($id);
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Fitmomtobe';
            $thumbFilePath = '/uploaded_images/Fitmomtobe';
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }
            $thumbnail = time() .'.png';
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $status = $files->move(public_path() . $filePath, $profilevideo);
            
            if ($status) {
                VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath, $thumbnail, 2, 640, 480);
               /* $ffprobe = FFMpeg\FFProbe::create();
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";

            if (file_exists(public_path().$filePath.'/' .$fitmomtobeOld['video'])) {
                unlink(public_path().$filePath.'/' .$fitmomtobeOld['video']);
            }
            if (file_exists(public_path().$thumbFilePath.'/' .$fitmomtobeOld['video_thumb'])) {
                unlink(public_path().$thumbFilePath.'/' .$fitmomtobeOld['video_thumb']);
            }
        } else if ($all['video_path'] != '') {
            $all['video'] = $all['video_path'];
        }
        if($fitmomtobe->update($all)) {
            /*if($files = $request->file('video')) {
                if (file_exists(public_path().$filePath.'/' .$fitmomtobeOld['video'])) {
                    unlink(public_path().$filePath.'/' .$fitmomtobeOld['video']);
                }
                if (file_exists(public_path().$thumbFilePath.'/' .$fitmomtobeOld['video_thumb'])) {
                    unlink(public_path().$thumbFilePath.'/' .$fitmomtobeOld['video_thumb']);
                }
            }*/
            return Redirect('fit_mom_2_b')->with('Success', __('admin_message.FITMOMTOBE_EDITED'));
        } else {
            return Redirect('fit_mom_2_b')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Fitmomtobe  $fitmomtobe
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Fitmomtobe $fitmomtobe)
    {
        $fitmomtobe = Fitmomtobe::find($id);
        $filePath = '/uploaded_videos/Fitmomtobe';
        if (file_exists(public_path().$filePath.'/' .$fitmomtobe['video'])) {
            unlink(public_path().$filePath.'/' .$fitmomtobe['video']);
        }
        if($fitmomtobe->delete()) {
            return Redirect('fit_mom_2_b')->with('Success', __('admin_message.FITMOMTOBE_DELETE'));
        } else {
            return Redirect('fit_mom_2_b')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }
}



?>