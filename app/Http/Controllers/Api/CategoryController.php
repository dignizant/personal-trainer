<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Lang;
use Config;
use App\Category;
use App\SubCategory;
use App\Nutrition;
use App\StayingFit;
use App\Barbielegs;
use App\RecipeImage;
use App\Pages;
use App\Fitmomtobe;
use Validator, DB, Hash, Mail;

class CategoryController extends Controller
{
    public function main_category(Request $request)
    {
        $category = [
            [
                'id' => 1,
                'name' => 'Nutrition',
            ],[
                'id' => 2,
                'name' => 'Staying fit',
            ],[
                'id' => 3,
                'name' => 'Barbielegs',
            ],[
                'id' => 4,
                'name' => 'Fit Mom-2-B',
            ]
        ];
       
       // $category = Category::select('id','category_name')->where('status',Config::get('constants.STATUS_ACTIVE'))->get();
        if (isset($category) && (count($category) != 0)) {
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.CATEGORY_LIST'), 'data' => $category], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.CATEGORY_LIST_NOT_AVAILABLE')], 200);
        }
    }

    public function sub_category(Request $request)
    {
        $category_id = $request->category_id;

        $credentials = $request->only('category_id');
        
        $rules = [
            'category_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $sub_category = SubCategory::select('id','category_id','category_name')->where('category_id',$category_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->get();
        foreach($sub_category as $key => $sub_cat) {
            if($sub_cat->category_id == 1){
                $recipe_count = Nutrition::where('sub_category_id',$sub_cat->id)->where('status',Config::get('constants.STATUS_ACTIVE'))->count();
                $sub_category[$key]['count'] = $recipe_count;
            } else if($sub_cat->category_id == 2){
                $recipe_count = StayingFit::where('sub_category_id',$sub_cat->id)->where('status',Config::get('constants.STATUS_ACTIVE'))->count();
                $sub_category[$key]['count'] = $recipe_count;
            } else if($sub_cat->category_id == 3){
                $recipe_count = Barbielegs::where('sub_category_id',$sub_cat->id)->where('status',Config::get('constants.STATUS_ACTIVE'))->count();
                $sub_category[$key]['count'] = $recipe_count;
            } else if($sub_cat->category_id == 4){
                $recipe_count = Fitmomtobe::where('sub_category_id',$sub_cat->id)->where('status',Config::get('constants.STATUS_ACTIVE'))->count();
                $sub_category[$key]['count'] = $recipe_count;
            } else {
                $sub_category[$key]['count'] = 0;
            }
        }
     
        if (isset($sub_category) && (count($sub_category) != 0)) {
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.CATEGORY_LIST'), 'data' => $sub_category], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.CATEGORY_LIST_NOT_AVAILABLE')], 200);
        }
    }

    /* public function nutrition_category(Request $request)
    {
        $nutrition_category = [
            [
                'id' => 1,
                'name' => 'Small Plates',
            ],[
                'id' => 2,
                'name' => 'Medium Plates',
            ],[
                'id' => 3,
                'name' => 'Big Plates',
            ]
        ];
        if (!empty($nutrition_category)) {
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.NUTRITION_CATEGORY_LIST'), 'data' => $nutrition_category], 401);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.NUTRITION_CATEGORY_LIST_NOT_AVAILABLE')], 401);
        }
    } */
    public function nutrition_list(Request $request)
    {
        $sub_category_id = $request->sub_category_id;
        $page_offset = $request->page_offset;

        $credentials = $request->only('sub_category_id');
        
        $rules = [
            'sub_category_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $nutrition_count = Nutrition::select('id','sub_category_id','title')->where('sub_category_id',$sub_category_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->get();

        $list_query = Nutrition::select('id','sub_category_id','title');
        $list_query = $list_query->where('sub_category_id',$sub_category_id);
        $list_query = $list_query->where('status',Config::get('constants.STATUS_ACTIVE'));
        if(isset($page_offset) && $page_offset != ''){
            $list_query = $list_query->limit(Config::get('constants.PAGE_LIMIT'));
            $list_query = $list_query->offset($page_offset);
        }
        $nutrition_lists = $list_query->get();
        if (isset($nutrition_lists) && (count($nutrition_lists) != 0)) {
            foreach($nutrition_lists as $key => $nutrition_list){
                $recipe_image = RecipeImage::where('recipe_id',$nutrition_list->id)->get()->first();
                $image_path = public_path('/uploaded_images/Nutrition/'.$recipe_image->image);
                if(isset($recipe_image->image) && file_exists($image_path)){
                    $nutrition_lists[$key]->image = url('/uploaded_images/Nutrition/'.$recipe_image->image);;
                } else {
                    $nutrition_lists[$key]->image = url('/images/ic_video_big.png');
                }
            }

            $next_offset = $page_offset + Config::get('constants.PAGE_LIMIT');
            if (count($nutrition_count) > $next_offset) {
                $offset = $next_offset;
            } else {
                $offset = -1;
            }

            return response()->json(['flag' => 1, 'next_offset' => $offset , 'msg' => Lang::get('messages.NUTRITION_LIST'), 'data' => $nutrition_lists], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.NUTRITION_LIST_NOT_AVAILABLE')], 200);
        }

    }
    
    public function nutrition_detail(Request $request)
    {
        $nutrition_id = $request->nutrition_id;

        $credentials = $request->only('nutrition_id');
        
        $rules = [
            'nutrition_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $nutrition = Nutrition::where('id',$nutrition_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->first();
        if (isset($nutrition) ){
            $recipe_images = RecipeImage::select('id','image')->where('recipe_id',$nutrition->id)->get();
            foreach($recipe_images as $key => $recipe_image) {
                $image_path = public_path('/uploaded_images/Nutrition/'.$recipe_image->image);
                if(isset($recipe_image->image) && file_exists($image_path)){
                    $recipe_images[$key]->image = url('/uploaded_images/Nutrition/'.$recipe_image->image);
                } else {
                    $recipe_images[$key]->image = url('/images/no_image.png');
                }
            }
            $nutrition->image = $recipe_images;
            $nutrition->duration = $nutrition->duration.' min';
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.NUTRITION_DETAIL'), 'data' => $nutrition], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.NUTRITION_DETAIL_NOT_AVAILABLE')], 200);
        }

    }

    public function staying_fit_list(Request $request)
    {
        $sub_category_id = $request->sub_category_id;
        $page_offset = $request->page_offset;

        $credentials = $request->only('sub_category_id');
        
        $rules = [
            'sub_category_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $staying_fit_count = StayingFit::where('sub_category_id',$sub_category_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->get();

        $list_query = StayingFit::where('sub_category_id',$sub_category_id)
        ->where('status',Config::get('constants.STATUS_ACTIVE'));
        if(isset($page_offset) && $page_offset != ''){
            $list_query = $list_query->limit(Config::get('constants.PAGE_LIMIT'));
            $list_query = $list_query->offset($page_offset);
        }
        $staying_fit_lists = $list_query->get();

        if (isset($staying_fit_lists) && (count($staying_fit_lists) != 0)) {
            foreach($staying_fit_lists as $key => $staying_fit_list){
                $video_path = public_path('/uploaded_videos/Stayingfit/'.$staying_fit_list->video);
                $thumb_image = public_path('/uploaded_images/Stayingfit/'.$staying_fit_list->video_thumb);
                if(isset($staying_fit_list->video) && file_exists($video_path)){
                    $staying_fit_lists[$key]->video = url('/uploaded_videos/Stayingfit/'.$staying_fit_list->video);
                } else {
                    $staying_fit_lists[$key]->video = url('/images/ic_video_big.png');
                }
                if(isset($staying_fit_list->video_thumb) && file_exists($thumb_image)){
                    $staying_fit_lists[$key]->video_thumb = url('/uploaded_images/Stayingfit/'.$staying_fit_list->video_thumb);
                } else {
                    $staying_fit_lists[$key]->video_thumb = url('/images/ic_video_big.png');
                }
            }

            $next_offset = $page_offset + Config::get('constants.PAGE_LIMIT');
            if (count($staying_fit_count) > $next_offset) {
                $offset = $next_offset;
            } else {
                $offset = -1;
            }

            return response()->json(['flag' => 1,'next_offset' => $offset , 'msg' => Lang::get('messages.STAYING_FIT_LIST'), 'data' => $staying_fit_lists], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.STAYING_FIT_LIST_NOT_AVAILABLE')], 200);
        }

    }

    public function barbielegs_list(Request $request)
    {
        $sub_category_id = $request->sub_category_id;
        $page_offset = $request->page_offset;
        $credentials = $request->only('sub_category_id');
        
        $rules = [
            'sub_category_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $barbielegs_count = Barbielegs::where('sub_category_id',$sub_category_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->get();

        $list_query = Barbielegs::where('sub_category_id',$sub_category_id)
        ->where('status',Config::get('constants.STATUS_ACTIVE'));
        if(isset($page_offset) && $page_offset != ''){
            $list_query = $list_query->limit(Config::get('constants.PAGE_LIMIT'));
            $list_query = $list_query->offset($page_offset);
        }
        $barbielegs_lists = $list_query->get();
      
        if (isset($barbielegs_lists) && (count($barbielegs_lists) != 0) ){
            foreach($barbielegs_lists as $key => $barbielegs_list){
                $video_path = public_path('/uploaded_videos/Barbielegs/'.$barbielegs_list->video);
                $thumb_image = public_path('/uploaded_images/Barbielegs/'.$barbielegs_list->video_thumb);
                if(isset($barbielegs_list->video) && file_exists($video_path)){
                    $barbielegs_lists[$key]->video = url('/uploaded_videos/Barbielegs/'.$barbielegs_list->video);;
                } else {
                    $barbielegs_lists[$key]->video = url('/images/ic_video_big.png');
                }
                if(isset($barbielegs_list->video_thumb) && file_exists($thumb_image)){
                    $barbielegs_lists[$key]->video_thumb = url('/uploaded_images/Barbielegs/'.$barbielegs_list->video_thumb);;
                } else {
                    $barbielegs_lists[$key]->video_thumb = url('/images/ic_video_big.png');
                }
            }

            $next_offset = $page_offset + Config::get('constants.PAGE_LIMIT');
            if (count($barbielegs_count) > $next_offset) {
                $offset = $next_offset;
            } else {
                $offset = -1;
            }

            return response()->json(['flag' => 1, 'next_offset' => $offset ,'msg' => Lang::get('messages.BARBIELEGS_LIST'), 'data' => $barbielegs_lists], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.BARBIELEGS_LIST_NOT_AVAILABLE')], 200);
        }

    }

    public function fit_mom_2_b_list(Request $request)
    {
        $sub_category_id = $request->sub_category_id;
        $page_offset = $request->page_offset;
        $credentials = $request->only('sub_category_id');
        
        $rules = [
            'sub_category_id' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $fit_mom_2_b_count = Fitmomtobe::where('sub_category_id',$sub_category_id)->where('status',Config::get('constants.STATUS_ACTIVE'))->get();

        $list_query = Fitmomtobe::where('sub_category_id',$sub_category_id)
        ->where('status',Config::get('constants.STATUS_ACTIVE'));
        if(isset($page_offset) && $page_offset != ''){
            $list_query = $list_query->limit(Config::get('constants.PAGE_LIMIT'));
            $list_query = $list_query->offset($page_offset);
        }
        $fit_mom_2_b_lists = $list_query->get();
      
        if (isset($fit_mom_2_b_lists) && (count($fit_mom_2_b_lists) != 0) ){
            foreach($fit_mom_2_b_lists as $key => $val){
                $video_path = public_path('/uploaded_videos/Fitmomtobe/'.$val->video);
                $thumb_image = public_path('/uploaded_images/Fitmomtobe/'.$val->video_thumb);
                if(isset($val->video) && file_exists($video_path)){
                    $fit_mom_2_b_lists[$key]->video = url('/uploaded_videos/Fitmomtobe/'.$val->video);
                } else {
                    $fit_mom_2_b_lists[$key]->video = url('/images/ic_video_big.png');
                }
                if(isset($val->video_thumb) && file_exists($thumb_image)){
                    $fit_mom_2_b_lists[$key]->video_thumb = url('/uploaded_images/Fitmomtobe/'.$val->video_thumb);
                } else {
                    $fit_mom_2_b_lists[$key]->video_thumb = url('/images/ic_video_big.png');
                }
            }

            $next_offset = $page_offset + Config::get('constants.PAGE_LIMIT');
            if (count($fit_mom_2_b_count) > $next_offset) {
                $offset = $next_offset;
            } else {
                $offset = -1;
            }

            return response()->json(['flag' => 1, 'next_offset' => $offset ,'msg' => Lang::get('messages.FIT_MOM_2_B_LIST'), 'data' => $fit_mom_2_b_lists], 200);
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.FIT_MOM_2_B_LIST_NOT_AVAILABLE')], 200);
        }

    }

    public function pages(Request $request)
    {
        $page_name = $request->only('page_name');
        
        $rules = [
            'page_name' => 'required',
        ];
        $credentials = $request->only('page_name');
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $pages = Pages::where('page_name',$page_name)->first();
        if ($pages) {
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.PAGE_DETAIL'), 'data' => $pages], 200);
        } else {
            return response()->json([
                'flag' => 0, 'msg'=>  Lang::get('messages.PAGE_NOT_FOUND')],200);
        }
    }
}
