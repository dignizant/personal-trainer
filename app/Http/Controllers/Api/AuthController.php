<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use JWTAuth;
use Auth;
use Tymon\JWTAuth\Exceptions\JWTException;
use Validator, DB, Hash, Mail;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Support\Facades\Password;
use Illuminate\Mail\Message;
use Illuminate\Support\Str;
use Illuminate\Auth\Events\PasswordReset;
use Lang;

class AuthController extends Controller
{
    /**
     * API Register
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
        $credentials = $request->only('name', 'email', 'password');
        
        $rules = [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return $this->makeError(null, $validator->errors()->first());
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $name = $request->name;
        $email = $request->email;
        $password = $request->password;
        
        $user = User::create(['name' => $name, 'email' => $email, 'password' => Hash::make($password)]);
        $verification_code = str_random(30); //Generate verification code
        DB::table('user_verifications')->insert(['user_id'=>$user->id,'token'=>$verification_code]);
        $subject = "Email verification.";
        $username = $name;
        Mail::send('email.verify', ['username' => $username, 'verification_code' => $verification_code],
            function($mail) use ($email, $username, $subject){
              //  $mail->from(getenv('FROM_EMAIL_ADDRESS'), "From User/Company Name Goes Here");
                $mail->to($email, $username);
                $mail->subject($subject);
            });
        return response()->json(['flag'=> 1, 'msg'=> Lang::get('messages.SIGN_UP_SUCCESS')],200);
    }

    public function verifyUser($verification_code)
    {
        $check = DB::table('user_verifications')->where('token',$verification_code)->first();
        if(!is_null($check)){
            $user = User::find($check->user_id);
            if($user->is_verified == 1){
                /* return response()->json([
                    'flag'=> 1,
                    'msg'=> Lang::get('messages.ACCOUNT_ALREADY_VERIFIED')
                ],200); */
                return Redirect('verify')->with('Warning', __('messages.ACCOUNT_ALREADY_VERIFIED'));
            }
            $user->update(['is_verified' => 1]);
            DB::table('user_verifications')->where('token',$verification_code)->delete();
           /*  return response()->json([
                'flag'=> 1,
                'msg'=> Lang::get('messages.SUCCESSFULLY_VERYFIED')
            ],200); */
            return Redirect('verify')->with('Success', __('messages.SUCCESSFULLY_VERYFIED'));
        }
        //return response()->json(['flag'=> 0, 'msg'=> Lang::get('messages.VERIFIED_CODE_INVALID')],200);
        return Redirect('verify')->with('Error', __('messages.VERIFIED_CODE_INVALID'));
    }
    public function verify()
    {
        return view('layouts.user-verify');
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        
        $rules = [
            'email' => 'required|email',
            'password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }
        
        $credentials['is_verified'] = 1;
        $credentials['is_admin'] = 0;
        try {
            // attempt to verify the credentials and create a token for the user
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json(['flag' => 0, 'msg' => Lang::get('messages.VERYFY_EMAIL_ERROR')], 200);
            }
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.LOGIN_FAILED')], 200);
        }
        // all good so return the token
        $user = [
            'username' => Auth::user()->name,
            'email' => Auth::user()->email,
        ];
        return response()->json(['flag' => 1, 'msg' => Lang::get('messages.LOGIN_SUCCESS'), 'access_token'=> $token , 'data' => $user], 200);
    }
    /**
     * Log out
     * Invalidate the token, so user cannot use it anymore
     * They have to relogin to get a new token
     *
     * @param Request $request
     */
    public function logout(Request $request) {
        try {
            JWTAuth::invalidate($request->bearerToken());
            return response()->json(['flag' => 1, 'msg'=> Lang::get('messages.LOGOUT_SUCCESS')],200);
        } catch (JWTException $e) {
            // something went wrong whilst attempting to encode the token
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.LOGOUT_FAIL')], 200);
        }
       
    }

    public function forgot_password(Request $request)
    {
        $email = $request->email;
        $user = User::where('email', $request->email)->first();
        if (!$user) {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.EMAIL_NOT_FOUND')], 200);
        }
        try {
            $subject = "Forgot Password.";
            $username = $user->name;
            $token = app('auth.password.broker')->createToken($user);
            Mail::send('email.forgot_password', ['username' => $username, 'url' => url('password-reset', $token)],
                function($mail) use ($email, $username, $subject){
                  //  $mail->from(getenv('FROM_EMAIL_ADDRESS'), "From User/Company Name Goes Here");
                    $mail->to($email, $username);
                    $mail->subject($subject);
                });
        } catch (\Exception $e) {
            //Return with error
            $error_message = $e->getMessage();
            return response()->json(['flag' => 0, 'msg' => $error_message], 200);
        }
        return response()->json([
            'flag' => 1, 'msg'=> Lang::get('messages.RESET_PASSWORD_LINK')
        ],200);
    }

    public function change_password(Request $request)
    {
        $credentials = $request->only('old_password', 'new_password');
        
        $rules = [
            'old_password' => 'required',
            'new_password' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $old_password = $request->old_password;
        $new_password = $request->new_password;
        $user = Auth::user();
      
        if(Hash::check($old_password, $user->password)){
            $user->password = Hash::make($new_password);
            if($user->update()){
                return response()->json(['flag' => 1, 'msg' => Lang::get('messages.PASSWORD_CHANGE_SUCCESSFULLY')], 200);
            } else {
                return response()->json(['flag' => 0, 'msg' => Lang::get('messages.SOMETHING_WENT_WRONG')], 200);
            }
        } else {
            return response()->json(['flag' => 0, 'msg' => Lang::get('messages.OLD_PASSWORD_NOT_MATCH')], 200);
        }
    }

    public function user_detail(Request $request) {
        $user = Auth::user();
        if (!$user) {
            $error_message = "Your email address was not found.";
            return response()->json(['flag' => 0, 'msg' => $error_message], 200);
        } else {
            return response()->json([
                'flag' => 1, 'msg'=> Lang::get('messages.USER_DETAIL') , 'data' => $user
            ],200);
        }
    }
    public function update_profile(Request $request) {
        $credentials = $request->only('name');
        
        $rules = [
            'name' => 'required',
        ];
        $validator = Validator::make($credentials, $rules);
        if($validator->fails() && $validator->errors()->count() > 0) {
            //return response()->json(['flag'=> 0, 'msg'=> $validator->messages()], 200);
            return response()->json(['flag'=> 0, 'msg'=> $validator->errors()->first()],200);
        }

        $user = Auth::user();
        $user->name = $request->name;
        if ($user->save()) {
            return response()->json(['flag' => 1, 'msg' => Lang::get('messages.PROFILE_UPDATE'), 'data' => $user], 200);
        } else {
            return response()->json([
                'flag' => 0, 'msg'=>  Lang::get('messages.SOMETHING_WENT_WRONG')],200);
        }
    }
    public function passwordReset(Request $request, $token) {
        return view('auth.passwords.app-reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }
    public function reset(Request $request)
    {
        //some validation
        $validation =  [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ];
        $this->validate($request, $validation,[]);

        $password = $request->password;
        $request = $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
        $response = Password::broker()->reset($request, function ($user, $password) {
            $user->password = Hash::make($password);
            $user->setRememberToken(Str::random(60));
            $user->save();
            event(new PasswordReset($user));
        });

        return $response == Password::PASSWORD_RESET
        ? $this->sendResetResponse($response ,$request)
        : $this->sendResetFailedResponse($request, $response);
    }
 
    protected function sendResetFailedResponse($request, $response)
    {
        //return trans($response);
        return redirect()->back()
                    //->withInput($request['email'])
                    ->withErrors(['email' => trans($response)]);
    }
   
    protected function sendResetResponse($response , $request)
    {
        return redirect($this->redirectPath())
        //->with('status', trans($response));
        ->with('Success', 'Password change successfully');

       // return redirect('verify')->with('Success', 'Password change successfully');
    }
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : 'verify';
    }
}
