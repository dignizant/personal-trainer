<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class CommonController extends Controller
{

    public function ajaxGetCropModal(Request $request)
    {
        //
        $all = $request->all();
        $data['image_data'] = @$all['image_data'];
		$data['ratioWidth'] = @$all['ratioWidth'];
		$data['rationHeight'] = @$all['rationHeight'];
		$data['preview_image'] = @$all['preview_image'];
		$data['input_image'] = @$all['input_image'];
		$data['hidden_input_image'] = @$all['hidden_input_image'];
		$data['prev_image'] = @$all['prev_image'];
        $data['mimeType'] = @$all['mimeType'];
       

        return view('admin.common.ajax_get_crop_modal',compact('data'));
    }
}