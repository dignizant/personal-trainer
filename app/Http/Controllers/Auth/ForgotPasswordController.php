<?php

namespace App\Http\Controllers\Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Support\Facades\Password;
use App\User;

class ForgotPasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset emails and
    | includes a trait which assists in sending these notifications from
    | your application to your users. Feel free to explore this trait.
    |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    public function sendResetLinkEmail(Request $request)
    {
        
         $this->validate($request, ['email' => 'required|email']);

        // We will send the password reset link to this user. Once we have attempted
        // to send the link, we will examine the response then see the message we
        // need to show to the user. Finally, we'll send out a proper response.
        $user = User::where('email' , $request->email)->first();
        
        if(isset($user) && $user->is_admin == 1){
            $response = $this->broker()->sendResetLink(
                $request->only('email')
            );
    
            return $response == Password::RESET_LINK_SENT
                        ? $this->sendResetLinkResponse($response)
                        : $this->sendResetLinkFailedResponse($request, $response);
        } else {
            return back()->with('Error', __('admin_message.EMAIL_NOT_REGISTERED'));
        }
        
    }

    protected function sendResetLinkResponse($response)
    {
        return back()->with('status', trans($response));
    }

    
    protected function sendResetLinkFailedResponse(Request $request, $response)
    {
        return back()->withErrors(
            ['email' => trans($response)]
        );
    }
}
