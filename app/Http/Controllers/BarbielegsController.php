<?php

namespace App\Http\Controllers;
use Yajra\Datatables\Datatables;
use App\Barbielegs;
use App\http\Requests;
use Illuminate\Http\Request;
use App\SubCategory;
use VideoThumbnail;
//use FFMpeg;

class BarbielegsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        if ($request->ajax()) {
            $barbielegs = Barbielegs::with(['category_details'])->get();
            
            return Datatables::of($barbielegs)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
                        $btn = '<a href="barbielegs/'.$row->id.'/edit" class="edit btn btn-primary btn-sm">Edit</a>
                                <form class="form-group" action="barbielegs/'.$row->id.'" method="POST">'.method_field('DELETE').' '.csrf_field().'
                                <button type="submit" class="delete btn btn-danger btn-sm" style="border:none">Delete</button></form>';
                        return $btn;
                    })
                    ->addColumn('sub_category_name', function($row1){
                        return $row1->category_details->category_name;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }
        return view('admin.barbielegs.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $subCategories = SubCategory::where('category_id', 3)->where('status','active')->get();
        return view('admin.barbielegs.create',compact('subCategories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'sub_category_id' => 'required',
            'title' => 'required',
            'video' => 'required|mimes:mp4|max:61440'
        ]);
        $all = $request->all();
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Barbielegs';
            $thumbFilePath = '/uploaded_images/Barbielegs';
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $thumbnail = time() .'.png';
            $status = $files->move(public_path() . $filePath, $profilevideo);
            
            if ($status) {
                VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath.'/', $thumbnail, 2, 640, 480);
                /*$ffprobe = FFMpeg\FFProbe::create();
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";
         }
        if($barbielegs = Barbielegs::create($all)) {
            return Redirect('barbielegs')->with('Success', __('admin_message.BARBIELEGS_ADDED'));
        } else {
            return Redirect('barbielegs')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Barbielegs  $barbielegs
     * @return \Illuminate\Http\Response
     */
    public function show(Barbielegs $barbielegs)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Barbielegs  $barbielegs
     * @return \Illuminate\Http\Response
     */
    public function edit($id, Barbielegs $barbielegs)
    {
        $barbielegs = Barbielegs::findOrFail($id);
        $subCategories = SubCategory::where('category_id', 3)->where('status','active')->get();
        return view('admin.barbielegs.edit',compact('subCategories','barbielegs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Barbielegs  $barbielegs
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request, Barbielegs $barbielegs)
    {
        //
        $validate_check_array = [
            'sub_category_id' => 'required',
            'title' => 'required',
        ];
        $all = $request->all();
        if (!$request->file('video') && $all['video_path'] == '') {
            $validate_check_array['video'] = 'required|mimes:mp4';
        } 
        $this->validate($request, $validate_check_array);
        $barbielegsOld = $barbielegs = Barbielegs::findOrFail($id);
        if ($files = $request->file('video')) {
            $filePath = '/uploaded_videos/Barbielegs';
            $thumbFilePath = '/uploaded_images/Barbielegs';
            if (!is_dir(public_path() . $thumbFilePath)) {
                mkdir(public_path() . $thumbFilePath, 0777, true);
            }
            if (!is_dir(public_path() . $filePath)) {
                mkdir(public_path() . $filePath, 0777, true);
            }

            $thumbnail = time() .'.png';
            $profilevideo = time() . "." . $files->getClientOriginalExtension();
            $status = $files->move(public_path() . $filePath, $profilevideo);
            if ($status) {
                VideoThumbnail::createThumbnail($status, public_path() . $thumbFilePath, $thumbnail, 2, 640, 480);
               /* $ffprobe = FFMpeg\FFProbe::create();
                $duration = $ffprobe
                    ->format($status) // extracts file informations
                    ->get('duration');
                $all['duration'] = explode('.',\FFMpeg\Coordinate\TimeCode::fromSeconds($duration)->toSeconds())[0];*/
                $all['video_thumb'] = "$thumbnail";
            }
            $all['video'] = "$profilevideo";

            if (file_exists(public_path().$filePath.'/' .$barbielegsOld['video'])) {
                unlink(public_path().$filePath.'/' .$barbielegsOld['video']);
            }
            if (file_exists(public_path().$thumbFilePath.'/' .$barbielegsOld['video_thumb'])) {
                unlink(public_path().$thumbFilePath.'/' .$barbielegsOld['video_thumb']);
            }
        } else if ($all['video_path'] != '') {
            $all['video'] = $all['video_path'];
        }
        if($barbielegs->update($all)) {
            /* if($files = $request->file('video')) {
                if (file_exists(public_path().$filePath.'/' .$barbielegsOld['video'])) {
                    unlink(public_path().$filePath.'/' .$barbielegsOld['video']);
                }
                if (file_exists(public_path().$thumbFilePath.'/' .$barbielegsOld['video_thumb'])) {
                    unlink(public_path().$thumbFilePath.'/' .$barbielegsOld['video_thumb']);
                }
            } */
            return Redirect('barbielegs')->with('Success', __('admin_message.BARBIELEGS_EDITED'));
        } else {
            return Redirect('barbielegs')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Barbielegs  $barbielegs
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Barbielegs $barbielegs)
    {
        //
        $barbielegs = Barbielegs::find($id);
        $filePath = '/uploaded_videos/Barbielegs/';
        if (file_exists(public_path().$filePath.'/' .$barbielegs['video'])) {
            unlink(public_path().$filePath.'/' .$barbielegs['video']);
        }
        if($barbielegs->delete()) {
            return Redirect('barbielegs')->with('Success', __('admin_message.BARBIELEGS_DELETE'));
        } else {
            return Redirect('barbielegs')->with('Error', __('admin_message.SOMETHING_WENT_WRONG'));
        }
    }
}
