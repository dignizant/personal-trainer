<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    protected $fillable = [
        'category_id', 'category_name', 'status'
    ];

    // public function category_details()
    // {
    //     return $this->belongsTo('App\Category','category_id','id');
    // }
}
