<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Nutrition extends Model
{
    //
    protected $fillable = [
        'sub_category_id', 'title','description','duration', 'status'
    ];
    protected $table = 'nutrition';

    public function category_details()
    {
        return $this->belongsTo('App\SubCategory','sub_category_id','id');
    }

    public function recipie_images()
    {
        return $this->hasMany('App\RecipeImage','recipe_id')->orderBy('id','asc');
    }
}
