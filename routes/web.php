<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   // return view('auth.login');
   return redirect('/home');
});

Auth::routes();

Route::get('/home', function () {
    // return view('auth.login');
    return redirect('/sub-category');
 });

//Route::get('/home', 'HomeController@index')->name('home');
Route::get('user/verify/{verification_code}', 'Api\AuthController@verifyUser');
Route::get('password-reset/{token}', 'Api\AuthController@passwordReset');
Route::post('password-reset', 'Api\AuthController@reset');
Route::get('verify', 'Api\AuthController@verify');
Route::group(['middleware' => 'auth'], function(){
    Route::resource('sub-category', 'SubCategoryController');
    Route::resource('users', 'UserController');
    Route::resource('barbielegs', 'BarbielegsController');
    Route::resource('nutrition', 'NutritionController');
    Route::resource('staying_fits', 'StayingFitController');
    Route::post('ajax_get_crop_modal','CommonController@ajaxGetCropModal');
    Route::delete('nutrition/remove_image/{id}', 'NutritionController@removeImage');
    Route::resource('pages', 'PagesController');
    Route::resource('fit_mom_2_b', 'FitmomtobeController');
});