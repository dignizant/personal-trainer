<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

Route::post('register', 'Api\AuthController@register');
Route::post('login', 'Api\AuthController@login');
Route::post('forgot-password', 'Api\AuthController@forgot_password');
Route::group(['middleware' => ['jwt.verify']], function() {
    Route::post('logout', 'Api\AuthController@logout');
    Route::post('change-password', 'Api\AuthController@change_password');
    Route::post('user-detail', 'Api\AuthController@user_detail');
    Route::post('update-profile', 'Api\AuthController@update_profile');
    Route::post('main-category', 'Api\CategoryController@main_category');
    Route::post('nutrition-category', 'Api\CategoryController@nutrition_category');
    Route::post('sub-category', 'Api\CategoryController@sub_category');
    Route::post('nutrition-list', 'Api\CategoryController@nutrition_list');
    Route::post('nutrition-detail', 'Api\CategoryController@nutrition_detail');
    Route::post('staying-fit-list', 'Api\CategoryController@staying_fit_list');
    Route::post('barbielegs-list', 'Api\CategoryController@barbielegs_list');
    Route::post('pages', 'Api\CategoryController@pages');
    Route::post('fit-mom-2-b-list', 'Api\CategoryController@fit_mom_2_b_list');
});
