$(document).ready(function() {
    $('#fitmom_to_be_form').validate({
        rules: {
            sub_category_id: {
                required: true,
            },
            title: {
                required: true,
            },
            video: {
                required: function(element) {
                    return ('' + $("#video_path").val() == "");
                },
                accept: "mp4"
            },
        },
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();

        },
        messages: {
            sub_category_id: {
                required: 'The sub category selection is required',
            },
            title: {
                required: 'The title is required',
            },
            video: {
                required: 'The video is required',
                accept: 'File must be MP4',
            },
        },
        errorPlacement: function(error, element) {
            if ($(element).hasClass('select2')) {
                var select2Element = $(element).next('.select2-container');
                $(error).insertAfter(select2Element);
            } else if ($(element).hasClass('filestyle')) {
                $(error).insertAfter('#remove_profile');
            } else {
                $(error).insertAfter(element);
            }
            $(error).addClass('text-danger');
        },
        highlight: function(element) {
            $(element).closest('.form-control').removeClass('has-success-form').addClass('has-error-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-success-form').addClass('has-error-form');
            }
        },
        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('has-error-form').addClass('has-success-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-error-form').addClass('has-success-form');
            }
        }
    });
});