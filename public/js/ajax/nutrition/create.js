$(document).ready(function() {
    $('#nutrition_form').validate({
        ignore: "input[type='text']:hidden",
        rules: {
            sub_category_id: {
                required: true,
            },
            title: {
                required: true,
            },
            description: {
                required: true,
            },
            duration: {
                required: true,
                number: true
            },
            'picture[]': {
                required: true
            },
        },
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();

        },
        messages: {
            sub_category_id: {
                required: 'The sub category selection is required',
            },
            title: {
                required: 'The title is required',
            },
            description: {
                required: 'The description is required',
            },
            duration: {
                required: 'The duration is required',
                number: 'Allow only digites',
            },
            'picture[]': {
                required: 'Add atlist one image of recipe'
            },
        },
        errorPlacement: function(error, element) {
            if ($(element).hasClass('select2')) {
                var select2Element = $(element).next('.select2-container');
                $(error).insertAfter(select2Element);
            } else if ($(element).hasClass('filestyle')) {
                $(error).insertAfter('#remove_profile');
            } else {
                $(error).insertAfter(element);
            }
            $(error).addClass('text-danger');
        },
        highlight: function(element) {
            $(element).closest('.form-control').removeClass('has-success-form').addClass('has-error-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-success-form').addClass('has-error-form');
            }
        },
        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('has-error-form').addClass('has-success-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-error-form').addClass('has-success-form');
            }
        },
        submitHandler: function(form) {
            $("#btnSave").attr('disabled', 'true');
            var picture = '';
            var form_data = new FormData();
            $("input[name='picture[]']").each(function() {
                picture_val = $(this).val();
                if (picture_val != '') {
                    var block = picture_val.split(";");
                    var contentType = block[0].split(":")[1];
                    var realData = block[1].split(",")[1];
                    picture = b64toBlob(realData, contentType);
                    form_data.append("images[]", picture);
                }
            });
            var image_id_array = [];
            $("input[name='images_id[]']").each(function() {
                image_id_array.push($(this).val());
            });

            title = $("#title").val();
            duration = $("#duration").val();
            description = $("#description").val();
            sub_category_id = $("#sub_category_id").val();
            status = $("#status").val();
            
            form_data.append("title", title);
            form_data.append("duration", duration);
            form_data.append("description", description);
            form_data.append("sub_category_id", sub_category_id);
            form_data.append("image_id_array", image_id_array);
            form_data.append("status", status);
            form_data.append("_method", post_method);
            form_data.append("_token", csrf_token);
            $.ajax({
                url: $('#nutrition_form').attr('action'),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(result) {
                    window.location.href = default_url;
                }
            });
            setTimeout(function() { $("#btnSave").removeAttr('disabled'); }, 4000);
            return false;
        }
    });

    $('#btnAddMore').click(function() {
        checkBuffer = $("#hidforBuffered").val();
        for (i = 0; i <= checkBuffer; i++) {
            if ($("#newDivImg" + i).length > 0) {
                if ($("#hdn_picture" + i).val() == '') {
                    alert('Please select the image first');
                    return false;
                }
            }
        }

        row_count = parseInt($("#hidforBuffered").val());
        row_count = row_count + 1;
        $("#hidforBuffered").val(row_count);
        $(".all_images").append('<div class="col-md-2" id="newDivImg' + row_count + '">' +
            '<div class="img_hover_edit image-item">' +
            '<input type="file" class="picture_input' + row_count + '" onchange="display_image(this,' + row_count + ')" name="picture_input' + row_count + '" value="" />' +
            '<input  type="hidden" name="picture[]" id="hdn_picture' + row_count + '" value="" />' +
            '<input  type="hidden" name="images_id[]" id="hdn_images_id' + row_count + '" value="0" />' +
            '<input type="hidden" id="hidPreImg' + row_count + '" name="hidPreImg' + row_count + '" value="' + img_url + '"/>' +
            '<a class="image-item-hover edit" id="editImg' + row_count + '" style="display:none"><span class="fa fa-edit fa-5x"></span></a>' +
            '<img src="' + img_url + '" id="preview_picture' + row_count + '" alt="Alternate Text" />' +
            '<a href="javascript:void(0)" onclick="deletenewDyanmicImage(' + row_count + ')" id="del_btn"  data-id="' + row_count + '"  class="del_icon close">' +
            '<i class="fa fa-times"></i>' +
            '</a>' +
            '</div>' +
            '</div>');
        $("#btnAddMore").attr('disabled', 'disabled');
    });
});

function display_image(input, temp, add_edit) {
    var size_in_bit = input.files[0].size;
    var size_in_kb = (size_in_bit / 1024);
    var size_in_mb = (size_in_kb / 1024);

    if (input.files && input.files[0] && input.files[0].type.match('image')) {
        $("#preview_picture" + temp).attr('src', loader_image);
        var reader = new FileReader();
        reader.onload = function(e) {
            mimeType = dataURLtoMimeType(reader.result);
            $.ajax({
                url: crop_modal_url,
                type: 'POST',
                data: {
                    "_token": csrf_token,
                    ratioWidth: 642,
                    rationHeight: 681,
                    preview_image: '#preview_picture' + temp,
                    input_image: '#picture_input' + temp,
                    hidden_input_image: '#hdn_picture' + temp,
                    prev_image: $("#hidPreImg" + temp).val(),
                    mimeType: mimeType
                },

                success: function(msg) {
                    $("#preview_picture" + temp).attr('src', loader_image);
                    $("#crop_modal_container").html(msg);

                    $('#crop_image_modal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });

                    $('#image').attr("src", e.target.result);
                    $("#editImg" + temp).css("display", "block");
                    $("#editImg" + temp).addClass("image-item-hover");
                    $(".btn_crop_done").click(function() {
                        $("#btnAddMore").removeAttr('disabled');
                    });
                }
            });
            return false;
        }
        reader.readAsDataURL(input.files[0]);
    } else {
        if (size_in_mb >= 2) {
            alert('Please Upload maximum 2 mb size not allow upload image more than 2 mb');
        } else {
            alert('Please Upload Only jpg or png or jpeg or bmp file');
        }
    }
}

function dataURLtoMimeType(dataURL) {
    var BASE64_MARKER = ';base64,';
    var data;
    if (dataURL.indexOf(BASE64_MARKER) == -1) {
        var parts = dataURL.split(',');
        var contentType = parts[0].split(':')[1];
        data = decodeURIComponent(parts[1]);
    } else {
        var parts = dataURL.split(BASE64_MARKER);
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;

        data = new Uint8Array(rawLength);

        for (var i = 0; i < rawLength; ++i) {
            data[i] = raw.charCodeAt(i);
        }
    }

    var arr = data.subarray(0, 4);
    var header = "";
    for (var i = 0; i < arr.length; i++) {
        header += arr[i].toString(16);
    }
    switch (header) {
        case "89504e47":
            mimeType = "image/png";
            break;
        case "47494638":
            mimeType = "image/gif";
            break;
        case "ffd8ffe0":
        case "ffd8ffe1":
        case "ffd8ffe2":
            mimeType = "image/jpeg";
            break;
        default:
            mimeType = ""; // Or you can use the blob.type as fallback
            break;
    }
    return mimeType;
}

function deletenewDyanmicImage(div_id) {
    $("#newDivImg" + div_id).remove();
    $("#btnAddMore").removeAttr('disabled');
}

function remove_image(image_id, del_key) {
    var del_id = image_id;

    if (del_id == '') {
        alert('Please select items to remove.');
        return false;
    } else {
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this data!",
                type: "error",
                showCancelButton: true,
                confirmButtonClass: 'btn-danger waves-effect waves-light',
                confirmButtonText: 'Delete'
            },
            function(isConfirm) {
                if (isConfirm) {
                    var form_data = {
                        "_token": csrf_token,
                        "_method": "DELETE"
                    };
                    $.ajax({
                        url: remove_image_url + '/' + del_id,
                        type: "POST",
                        data: form_data,
                        success: function(data) {
                            $("#newDivImg" + del_key).remove();
                            $("#btnAddMore").removeAttr('disabled');
                        }
                    });
                } else {
                    swal("Cancelled", "Your data is safe", "error");
                }
            });
    }
}

function b64toBlob(b64Data, contentType, sliceSize) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    var byteCharacters = atob(b64Data);
    var byteArrays = [];

    for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
        var slice = byteCharacters.slice(offset, offset + sliceSize);

        var byteNumbers = new Array(slice.length);
        for (var i = 0; i < slice.length; i++) {
            byteNumbers[i] = slice.charCodeAt(i);
        }

        var byteArray = new Uint8Array(byteNumbers);

        byteArrays.push(byteArray);
    }

    var blob = new Blob(byteArrays, { type: contentType });
    return blob;
};