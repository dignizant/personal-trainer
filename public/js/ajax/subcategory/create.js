$(document).ready(function() {
    $('#sub-category-form').validate({
        rules: {
            category_id: {
                required: true,
            },
            category_name: {
                required: true,
            },
        },
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();

        },
        messages: {
            category_id: {
                required: 'The category selection is required',
            },
            category_name: {
                required: 'The Sub category is required',
            },
        },
        errorPlacement: function(error, element) {
            if ($(element).hasClass('select2')) {
                var select2Element = $(element).next('.select2-container');
                $(error).insertAfter(select2Element);
            } else if ($(element).hasClass('filestyle')) {
                $(error).insertAfter('#remove_profile');
            } else {
                $(error).insertAfter(element);
            }
            $(error).addClass('text-danger');
        },
        highlight: function(element) {
            $(element).closest('.form-control').removeClass('has-success-form').addClass('has-error-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-success-form').addClass('has-error-form');
            }
        },
        unhighlight: function(element) {
            $(element).closest('.form-control').removeClass('has-error-form').addClass('has-success-form');
            if ($(element).hasClass('select2')) {
                $(element).next().find('.select2-selection').removeClass('has-error-form').addClass('has-success-form');
            }
        }
    });
});