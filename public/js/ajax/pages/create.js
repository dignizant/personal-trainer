$(document).ready(function() {
    $('#pages_form').validate({
        ignore: "input[type='text']:hidden",
        rules: {
            title: {
                required: true,
            },
            description: {
                required: true,
            }
        },
        invalidHandler: function(event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();

        },
        messages: {
            title: {
                required: 'The title is required',
            },
            description: {
                required: 'The description is required',
            }
        },
        errorPlacement: function(error, element) {
            if ($(element).hasClass('select2')) {
                var select2Element = $(element).next('.select2-container');
                $(error).insertAfter(select2Element);
            } else if ($(element).hasClass('filestyle')) {
                $(error).insertAfter('#remove_profile');
            } else {
                $(error).insertAfter(element);
            }
            $(error).addClass('text-danger');
        },
        submitHandler: function(form) {
            $("#btnSave").attr('disabled', 'true');
            var picture = '';
            var form_data = new FormData();

            title = $("#title").val();
            description = CKEDITOR.instances['description'].getData();
            
            form_data.append("title", title);
            form_data.append("description", description);
            form_data.append("_method", post_method);
            form_data.append("_token", csrf_token);
            $.ajax({
                url: $('#pages_form').attr('action'),
                data: form_data,
                type: 'POST',
                contentType: false,
                processData: false,
                dataType: 'json',
                success: function(result) {
                    console.log(result['success']);
                    if(result['success']) {
                        $("#about_success").html('<div id="success_about" class="alert alert-success"><span class="glyphicon glyphicon-ok"></span> '+result['success']+'</div>');
                       setTimeout(function () {
                               $("#success_about").css( "display", "none" );
                       },3000);
                    }
                    else {
                        $("#about_success").html('<div id="error_about" class="alert alert-danger"><span class="glyphicon glyphicon-ok"></span> '+result['error']+'</div>');
                       setTimeout(function () {
                               $("#error_about").css( "display", "none" );
                       },3000);
                    }
                    //window.location.href = default_url;
                }
            });
            setTimeout(function() { $("#btnSave").removeAttr('disabled'); }, 4000);
            return false;
        }
    });
});